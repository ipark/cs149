/*
 * hw0.4.c
 *
 *  Created on: Jan 28, 2019
 *      Author: ipark
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h> // malloc(), free()

void function1(char *str, char *ptr1[]);
void function2(char **ptr1, char **ptr2);

int main()
{
	// char array
 	/*
 	char str[] = "this is a test only - just a test";
	int i;
	for (i = 0; str[i] != '\0'; i++) {
		printf("%c", str[i]);
	}
	*/

	// pointer to char
	/*
	printf("\nPointer to character \n");
 	char *sPtr = "this is a test only - just a test";
	for ( ; *sPtr != '\0'; ++sPtr) {
		printf("%c", *sPtr);
	}
	*/

	// array of pointers of string
	/*
	char *cityPtr[] = {"Seoul", "Los Gatos", "San Jose"};
	int i; // index of array
	int c; // index of string
	for (i = 0; i < sizeof(cityPtr)/sizeof(char); i++) {
		c = 0;
		while (*(cityPtr[i] + c) != '\0') {
			printf("%c", *(cityPtr[i] + c));
			c++;
		}
		printf("\n");
	}
	*/

	char str[] = "this is      a test - just a test";
	char *ptr1[100], *ptr2[100];
	function1(str, ptr1);
	function2(ptr1, ptr2);
	return 0;
}

void function1(char *str, char *ptr1[])
{
	int i = 0; // index of ptr1 array
	char *p = strtok(str, " ");
	while (p != NULL) {
		ptr1[i++] = p;
		p = strtok(NULL, " ");
	}
    printf("ptr1===%p\t%s\n", ptr1, *ptr1);
    printf("&ptr1[0]===%p\t%s\n\n", &ptr1[0], ptr1[0]);
    printf("ptr1[0]===%p\t%s\n", ptr1[0], ptr1[0]);
    printf("&(*ptr1[0])===%p\t%s\n", &(*ptr1[0]), &(*ptr1[0]));
	//for (i = 0; i < sizeof(ptr1)/sizeof(char); i++) {
//		printf("%s\n", ptr1[i]);
//	}
}

void function2(char **ptr1, char **ptr2)
{
	// loop: print ptr1[i]
	int i;
	int len = sizeof(ptr1)/sizeof(char);
	for (i = 0; i < len; i++) {
		printf("%s\n", ptr1[i]);
	}
	// loop; for each ptr2[i], malloc and strcpy form ptr1[i], print ptr2[i]
	for (i = 0; i < len; i++) {
		ptr2[i] = (char *) malloc(sizeof(char) * strlen(ptr1[i]));
		// error handling: copy str only if malloc if successful
		if (ptr2[i] != NULL)
			strcpy(ptr2[i], ptr1[i]);
		printf("[ptr2]%s\n", ptr2[i]);
	}
	// loop: print the pointer value of ptr1[i] and ptr2[i]
	for (i = 0; i < len; i++) {
		printf("ptr1[i]=%p; ptr2[i]=%p\n", ptr1[i], ptr2[i]);
	}
	// loop: free each of ptr2[i]
	for (i = 0; i < len; i++) {
		free(ptr2[i]);
	}
}
