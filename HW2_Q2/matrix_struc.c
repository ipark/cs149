#include <stdio.h> // printf
#include <pthread.h> // pthread
#include <stdlib.h> // malloc(), free()

#define GRID_SIZE 9
#define SUB_GRID_SIZE 3
#define NUM_THREADS 11

int puzzle1[GRID_SIZE][GRID_SIZE] = {
    {1,8,3,5,7,6,2,4,9}, 
    {4,6,7,1,2,9,3,5,8}, 
    {9,2,5,4,3,8,7,2,6}, 
    {8,7,2,3,5,1,6,9,4}, 
    {6,9,4,7,8,2,1,3,5}, 
    {3,5,1,6,9,4,8,7,2}, 
    {5,1,9,2,6,0,4,8,7}, 
    {7,4,6,8,1,5,9,2,3}, 
    {2,3,8,9,4,7,5,6,1}};
typedef struct 
{
    int *puzzle;
    int row;
    int col;
    int validation_code;
} parameters;

void print_puzzle(int *p, int start_row, int start_col, int size)
{ 
    int row, col;
    printf("\n Given 9x9 Puzzle\n -----------------\n");
    for (row = start_row; row < start_row + size; row++) {
        for (col = start_col; col < start_col + size; col++) {
            printf(" %d", *(p + row * GRID_SIZE + col));
        }
        printf("\n");
    }
   printf(" -----------------\n");
}

int main() 
{
    parameters *data = (parameters *) malloc(sizeof(parameters)); 
    data->puzzle = (int *)puzzle1;
    print_puzzle(data->puzzle, 0, 0, GRID_SIZE);
    return 0;
}
