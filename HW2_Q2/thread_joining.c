#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <math.h>
#define NUM_THREADS 8

void *busyWork(void *t) 
{
    int     i;
    long    tid;
    double  result = 0.0;
    tid = (long) t; 
    printf("thread %ld starting...\n", tid);
    for (i = 0; i < 1000000; i++)
        result += i;
    printf("thread %ld done. result = %e\n", tid, result);

    pthread_exit((void *) t);
}

int main(int argc, char *argv[])
{
    pthread_t       threads[NUM_THREADS];
    pthread_attr_t  attr;
    int rc;
    long t;
    void *status;

    // initialize and set thread detached attr
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    for (t = 0; t < NUM_THREADS; t++) {
        printf("main: creating thread %ld\n", t);
        rc = pthread_create(&threads[t], &attr, busyWork, (void *) t);
    }

    // free attr and wait fot eh other threads
    pthread_attr_destroy(&attr);
    for (t = 0; t < NUM_THREADS; t++) {
        rc = pthread_join(threads[t], &status);
        printf("main: complete join w/ thread %ld w/ status of %ld\n", t, (long) status);
    }
    printf("main: program completes\n");
    pthread_exit(NULL);
}

