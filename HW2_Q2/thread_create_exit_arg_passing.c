#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> //sleep(3)
#define NUM_THREADS 8

char *msg[NUM_THREADS];

void *printHello(void *threadID)
{
    long taskid;
    sleep(1);
    taskid = (long) threadID;
    printf("thread %ld: %s\n", taskid, msg[taskid]);
    pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
    pthread_t   threads[NUM_THREADS];
    long        taskids[NUM_THREADS];
    int t, rc;
    msg[0] = "message0";
    msg[1] = "message1";
    msg[2] = "message2";
    msg[3] = "message3";
    msg[4] = "message4";
    msg[5] = "message5";
    msg[6] = "message6";
    msg[7] = "message7";

    for (t = 0; t < NUM_THREADS; t++) {
        taskids[t] = t;
        printf("pthread_create %d\n", t);
        rc = pthread_create(&threads[t], NULL, printHello, (void *) taskids[t]);
        if (rc) {
            printf("ERROR: return code from pthread_create() is %d\n", rc);
            exit(-1);
        }
    }
    pthread_exit(NULL); 
}
