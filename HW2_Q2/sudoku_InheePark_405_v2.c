/* CS149_HW2_Sudoku_Inhee Park_405_v2 (single structure data type) */
#include <stdio.h> // printf
#include <pthread.h> // pthread
#include <stdlib.h> // malloc(), free()
#include <stdint.h> // intptr_t

#define GRID_SIZE 9
#define SUB_GRID_SIZE 3
#define NUM_THREADS 11

int puzzle1[GRID_SIZE][GRID_SIZE] = {
    {1,8,3,5,7,6,2,4,9}, 
    {4,6,7,1,2,9,3,5,8}, 
    {9,2,5,4,3,8,7,2,6}, 
    {8,7,2,3,5,1,6,9,4}, 
    {6,9,4,7,8,2,1,3,5}, 
    {3,5,1,6,9,4,8,7,2}, 
    {5,1,9,2,6,0,4,8,7}, 
    {7,4,6,8,1,5,9,2,3}, 
    {2,3,8,9,4,7,5,6,1}};
int puzzle2[GRID_SIZE][GRID_SIZE] = {
    {5,9,6,7,1,4,3,2,8}, 
    {2,7,3,8,5,9,1,4,6}, 
    {8,1,4,2,3,6,7,5,9}, 
    {9,6,7,4,2,8,5,1,3}, 
    {1,2,5,3,9,7,6,8,4}, 
    {4,3,8,1,6,5,9,7,2}, 
    {3,5,1,6,8,2,4,9,7}, 
    {7,8,9,5,4,3,2,6,1}, 
    {6,4,2,9,7,1,8,3,5}};

typedef struct 
{
    int *puzzle;
    int tid;
    int row;
    int col;
} parameters;


void print_puzzle(int *p, int start_row, int start_col, int size)
{ 
    int row, col;
    printf("\n Given 9x9 Puzzle\n -----------------\n");
    for (row = start_row; row < start_row + size; row++) {
        for (col = start_col; col < start_col + size; col++) {
            printf(" %d", *(p + row * GRID_SIZE + col));
        }
        printf("\n");
    }
   printf(" -----------------\n");
}


void *row_checker(void *targs)
{
    parameters *data = (parameters *) targs;
    int tid = data->tid;
    void *validation_code = 0;

    printf("------------------------starting thread %d\n", tid);

    int row, col, val;
    for (row = 0; row < GRID_SIZE; row++) {
        int uniq_chcker[GRID_SIZE] = {0,0,0,0,0,0,0,0,0};
        for (col = 0; col < GRID_SIZE; col++) {
            val = *(data->puzzle + row * GRID_SIZE + col);
            if (val > 0) uniq_chcker[val - 1] = 1;
        }
        for (col = 0; col < GRID_SIZE; col++) {
            if (uniq_chcker[col] == 0) {
                printf(" row[%d], missing value %d\n", row, col + 1);
                validation_code = (int *) -1;
            }
        }
    }
    printf("------------------------exiting thread %d followed by  free()\n", tid);
    pthread_exit((void *) validation_code);
    free(data);

}


void *col_checker(void *targs)
{

    parameters *data = (parameters *) targs;
    int tid = data->tid;
    void *validation_code = 0;

    printf("------------------------starting thread %d\n", tid);

    int row, col, val;
    for (col = 0; col < GRID_SIZE; col++) {
        int uniq_chcker[GRID_SIZE] = {0,0,0,0,0,0,0,0,0};
        for (row = 0; row < GRID_SIZE; row++) {
            //val = *(p + row * GRID_SIZE + col);
            val = *(data->puzzle + row * GRID_SIZE + col);
            if (val > 0) uniq_chcker[val - 1] = 1;
        }
        for (row = 0; row < GRID_SIZE; row++) {
            if (uniq_chcker[row] == 0) {
                printf(" col[%d], missing value %d\n", col, row + 1);
                validation_code = (int *) -1;
            }
        }
    }
    printf("------------------------exiting thread %d followed by  free()\n", tid);
    pthread_exit((void *) validation_code);
    free(data);

}

void *subgrid_checker(void *targs)
{

    parameters *data = (parameters *) targs;
    int tid = data->tid;
    void *validation_code = 0;
    int start_row = data->row;
    int start_col = data->col;
    int row, col, val;

    printf("------------------------starting thread %d\n", tid);

    int uniq_chcker[GRID_SIZE] = {0,0,0,0,0,0,0,0,0};
    for (row = start_row; row < start_row + SUB_GRID_SIZE; row++) {
        for (col = start_col; col < start_col + SUB_GRID_SIZE; col++) {
            val = *(data->puzzle + row * GRID_SIZE + col);
            if (val < 1  || val > 9)  {
                printf(" puzzle[%d][%d] = %d, not between 1 and 9\n", 
                        col, row, val);
                validation_code = (int *) -1;
            }
            else { // 1 <= val <= 9
                if (uniq_chcker[val - 1] > 0) {
                    printf(" puzzle[%d][%d] = %d, duplicate value\n", 
                            col, row, val);
                    validation_code = (int *) -1;
                }
                else {
                    uniq_chcker[val - 1] = 1;
                }
            }
        } // EO-for-col
    } // EO-for-row
    printf("------------------------exiting thread %d followed by  free()\n", tid);
    pthread_exit((void *) validation_code);
    free(data);

}

void sudoku(int *p) 
{
    pthread_t threads[NUM_THREADS];
    int t = 0;
    parameters *data;

    // print the given puzzle
    print_puzzle((int *) p, 0, 0, GRID_SIZE);

    // creating 11 threads
    // thread 1
    // row-duplicate-checker                                      
    data = (parameters *) malloc(sizeof(parameters));
    data->puzzle = (int *) p;
    data->tid = t;
    printf("------------------------creating thread %d\n", t);
    pthread_create(&threads[t], NULL, row_checker, (void *)data);
    t++;

    // col-duplicate-checker 
    // thread 2
    data = (parameters *) malloc(sizeof(parameters));
    data->puzzle = (int *) p;
    data->tid = t;
    printf("------------------------creating thread %d\n", t);
    pthread_create(&threads[t], NULL, col_checker, (void *)data);
    t++;

    // 3x3-subgrid-chcker
    // thread 3-11
    int sub_row, sub_col;
    int start_row, start_col;
    for (sub_row = 0; sub_row < SUB_GRID_SIZE; sub_row++) {        
        for (sub_col = 0; sub_col < SUB_GRID_SIZE; sub_col++) {
            start_row = sub_row * SUB_GRID_SIZE;
            start_col = sub_col * SUB_GRID_SIZE;
            //printf("%d, %d\n", start_row, start_col);
            data = (parameters *) malloc(sizeof(parameters));
            data->puzzle = (int *) p;
            data->tid = t;
            data->row = start_row;
            data->col = start_col;
            printf("------------------------creating thread %d\n", t);
            pthread_create(&threads[t], NULL, subgrid_checker, (void *)data);
            t++;
        }
    }

    // joining 11 threads
    long exit_status = 0;
    void *validation_code;
    for (t = 0; t < NUM_THREADS; t++) {
        pthread_join(threads[t], &validation_code);
        printf("------------------------joining thread %d(exit-code=%ld)\n", t, (intptr_t)validation_code);
        exit_status += (intptr_t)validation_code;
    }
    if (exit_status == 0) 
        printf(" :::PASS:::this puzzle is VALID!\n");
    else
        printf(" :::FAIL:::this puzzle is NOT valid!\n");

}

int main()
{
    printf(" CS149 Sudoku from Inhee Park (405)\n");


    sudoku((int *) puzzle1);
    sudoku((int *) puzzle2);
    return 0;
}
