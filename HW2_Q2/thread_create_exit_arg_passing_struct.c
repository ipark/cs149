#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define NUM_THREADS 8

char *msg[NUM_THREADS];

typedef struct
{
    int thread_id;
    int sum;
    char *msg;
} thread_data;

thread_data thread_data_array[NUM_THREADS];

void *printHello(void *threadArg)
{
    int taskid, sum;
    char *hello_msg;
    thread_data *my_data;

    sleep(1);

    my_data = (thread_data *) threadArg;
    taskid = my_data->thread_id;
    sum = my_data->sum;
    hello_msg = my_data->msg;

    printf("thread %d : %s : sum=%d\n", taskid, hello_msg, sum);

    pthread_exit(NULL);
}

int main(int argc, char *argv[]) 
{
    pthread_t   threads[NUM_THREADS];
    int         *taskids[NUM_THREADS]; //?????
    int rc, t, sum;

    sum = 0;
    msg[0] = "message 0";
    msg[1] = "message 1";
    msg[2] = "message 2";
    msg[3] = "message 3";
    msg[4] = "message 4";
    msg[5] = "message 5";
    msg[6] = "message 6";
    msg[7] = "message 7";

    for (t = 0; t < NUM_THREADS; t++) {
        sum += t;
        thread_data_array[t].thread_id = t;
        thread_data_array[t].sum = sum;
        thread_data_array[t].msg = msg[t];

        printf("creating thread %d\n", t);
        pthread_create(&threads[t], NULL, printHello, 
                (void *) &thread_data_array[t]);
    }
    pthread_exit(NULL);
}
