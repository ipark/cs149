/* CS149-HW2-Q1 Inhee Park (405) */

#include <pthread.h>
#include <stdio.h>
#include <unistd.h> // fork()
#include <sys/types.h> // gettid()
#include <syscall.h>

void *runner(void *param); /* the thread */

int main(int argc, char *argv[])
{
    pthread_t tid; /* the thread identifier */

    pid_t pid; // proc id
    pid = fork(); /**************fork1 ******/
    
    if (pid == 0) {
        fork(); /****************fork2 ******/

        /* create the thread */
        pthread_create(&tid, NULL, runner, NULL);
        
        /* now wait for the thread to exit */
        pthread_join(tid,NULL);
        
    }
    fork(); /*******************fork3 ******/
    printf("Bye pid=%d\n", getpid());
}
    
void *runner(void *param) 
{
    printf("[pid=%d: tid=%lu]\n", getpid(), syscall(SYS_gettid));
	pthread_exit(0);
}
