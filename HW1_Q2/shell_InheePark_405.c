/* shell_InheePark_405.c */

#include <stdio.h>      // printf(3), fflush(3), fgets(3)
#include <unistd.h>     // fork(2), execvp(3)
#include <string.h>     // strtok(3)
#include <stdlib.h>     // malloc(3), free(3)
#include <sys/types.h>  // wait(2) for pid_t
#include <sys/wait.h>   // wait(2)

#define MAXLINE 80 // max length of command

// function prototype
void command_processor(char *str, char *args[]);

int main(void)
{
    char str[MAXLINE];  // char array for storing user input 
    char *args[MAXLINE / 2 + 1]; // pointer array for tokenized arguments
    int should_run = 1; // flag to indicate run if 1 (stop if 0)

    printf("CS149 Shell from INHEE PARK (405)\n"); // should be printed once

    while (should_run) {
        printf("INHEE-405> "); // prompt of simple shell
        fflush(stdout);

        // read stream from stdin and store into str[MAXLINE]
        fgets(str, MAXLINE, stdin);

        // if str is "exit\n", return 0
        if (strcmp(str, "exit\n") == 0) { 
            should_run = 0; // toggle flag to stop
        }
        else { // strcmp(str, "exit\n") !=0
            // call  the function to process user input
            // 1) to tokenize str to args; 
            // 2) execute args
            command_processor(str, args);
        }
    }
    return 0; // success
}

/* 
 * INPUT 1: char str[], storage of user input from stdin
 * INPUT 2: char *args[], to store tokenized string
 * 1) tokenize user input stored in str[], then storet to *args[] 
 * 2) append '\0' to the last element of *args[]  
 * 3) fork a child process using fork()
 * 4) child process will invoke execvp()
 * 5) if cmd included &, parent won't invoke wait()
 *    otherwise invoke wait(&status)
 */
void command_processor(char *str, char *args[])
{
    int i = 0; // index for args[]
    int n; // total number of element in args[]
    char *p; // temporary pointer for tokenizer
    int should_wait = 1; // flag to wait if 1 (don't wait if 0)
    /* for-loop: tokenize str with a set of deliminators
     * ' '(space) or '\t'(tab) or '\n'(newline)
     * invoke strtok with NULL argument to continue until retuning
     * NULL pointer
     */
    for (p = strtok(str, " \t\n"); p != NULL; p = strtok(NULL, " \t\n")) {
        if (strchr(p, '&') != NULL) { // if found & from token, exclude it
            should_wait = 0; // toggle wait flag 0, i.e. no wait
        }
        else { // save tokens  
            args[i++] = p; 
        }
    }
    n = i; // get the total number of elements in args[] 
    args[n] = '\0'; // append the NULL character to the end of args[]
    // (1) fork a child process using fork()
    int pid, status;
    pid = fork(); // fork for child process
    if (pid < 0) { // error
        printf("Fork error\n");
        exit(1);
    }
    // (2) child process will invoke execvp()
    else if (pid == 0) { // child process
        //for (i = 0; i <= n; i++)
        //    printf("args[%d] = %s\n", i, args[i]); 
        execvp(args[0], args); // execution of args[]
        printf("Execvp error\n"); // error checking when execvp fails
        exit(1);
    }
    // (3) if cmd included &, parent won't invoke wait()
    else { // parent process
        if (should_wait > 0) // should_wait = 1: wait
            wait(&status);
    }
}
