/*
 * hw0.2.c
 *
 *  Created on: Jan 28, 2019
 *      Author: ipark
 */


#include <stdio.h>
int main()
{
	int size = 100; // size
	int a[size]; // declare int array size of 100
	int i; // index

	// while-loop: initialization array 0-99
	while (i < 100) {
		a[i] = i;
		i++;
	}

	// for-loop: print value of each int of array
	for (i = 0; i < size; i++) {
		printf("%d\n", a[i]);
	}
	return 0;
}
