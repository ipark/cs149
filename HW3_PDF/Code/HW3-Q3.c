/* CS149-HW3-Q3 */

monitor bounded_buffer {
  /* MAX_ITEMS is a constant defined elsewhere; 
   * not a circular buffer */
  int items[MAX_ITEMS]; 
  /* # of items in the items array, 
   * 0 <= numItems <= MAX_ITEMS */ 
  int numItems = MAX_ITEMS; 
  condition full, empty;

  /* both produce() and consume() use numItems as index
   * to access the array */

  /* deposit the value v to the items array */
  void produce(int v); 
  /* remove an item from the items array, and return the value */
  int consume(); 
}

void produce(int v) {
  /** 
   * 1) to be thread-safe 
   *    use while-loop for condition checking 
   *    rather than if-statement;
   * 2) CV.wait() - 
   *    a thread should wait until condition(CV) to be met
   */
  while (numItems == MAX_ITEMS) { 
    full.wait(); // wait until full condition is met
  }
  /** 
   * insert item v and increase numItems
   */
  items[numItems++] = v;
  /**
   * CV.signal() - 
   * notify the other thread that their condition(CV) is met
   */
  empty.signal(); // signal that empty condition is met
}

int consume() {
  while (numItems == 0) {
    empty.wait(); // wait until empty condition is met
  }
  /** 
   * numItems was initialized to MAX_ITEMS; 
   * thus to access last element of the array items, 
   * should decrease numItems first (--numItems), 
   * and then access the element to return it
   */
  return items[--numItems];
  full.signal(); // signal that full condition is met
}
