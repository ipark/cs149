/* CS149-HW3-Q1 */

#define MAX_PROCS 511
int number_of_processes = 0;

int allocate_process() {
  int new_pid;
  mutex.acquire(); // [HW3-Q1b] mutex lock
  if (number_of_processes == MAX_PROCS) // [HW3-Q1] race condition
      return -1;
  else {
      ++number_of_processes;  // [HW3-Q1a] race condition
      return new_pid;
  } 
  mutex.release(); // [HW3-Q1b] mutex lock
}

void release_process() {
  mutex.acquire(); // [HW3-Q1b] mutex lock
  --number_of_processes; // [HW3-Q1a] race condition
  mutex.release(); // [HW3-Q1b] mutex lock
}
