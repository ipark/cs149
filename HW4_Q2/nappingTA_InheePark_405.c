/** 
 * CS149 - NappingTA - InheePark(405) 
 */
#include <pthread.h> // mutex, conditional variables
#include <stdio.h> // printf()
#include <stdlib.h> // rand_r()
#include <unistd.h> // sleep()
#include <time.h> // time()
#include <sys/types.h> // gettid()
#include <syscall.h> // SYS_gettid

/* the maximum time (in seconds) to sleep */
#define MAX_SLEEP_TIME 3

/* number of students */
#define NUM_OF_STUDENTS 4

/* number of TAs */
#define NUM_OF_TAS 2

/* # of eelps each student must receive before exit */ 
#define NUM_OF_HELPS 3

/* number of available seats */ 
#define NUM_OF_SEATS 2

/* mutex lock */
pthread_mutex_t student_mutex, ta_mutex;

/* # of waiting students, # of available TAs */
int waiting_students = 0;
int avail_tas = 0;

/* condition variables */
pthread_cond_t student_cond, ta_cond;

/** thread safe random number generator 
 * with seed value based on current time xor with 
 * thread pid (gettid) 
 */
int sleeping_time()
{
  /* seed value based on current time xor with tid */
  unsigned int seed = time(NULL) ^ syscall(SYS_gettid);
  /* thread safe random number generated with a seed value
   * whose resulting value is upto MAX_SLEEP_TIME 
   * calculated by mod followed by adding 1
   */
  return (int) (rand_r(&seed) % MAX_SLEEP_TIME) + 1;
}

/** When a student arrives, 
 * if there is any seat available,
 * the student sits down and notifies (or wakes up) TAs about arrival 
 * and then the student waits patiently for the help from any available TA.
 */
void *students_count(void *t)
{
  long s_id = (long) t; // student thread id
  int get_help = 0; // variable used for termination criteria
  int sec; // random number of seconds

  while (get_help < NUM_OF_HELPS) { // until getting NUM_OF_HELPS-1

    // student is doing programming
    sec = sleeping_time(); // random number of seconds
    printf("student[%ld, %d]: programs for %d seconds\n", s_id, get_help, sec); 
    fflush(NULL);
    sleep(sec); // programming 

    /** MUTEX_LOCK: pthread_cond_signal within mutex  **/
    pthread_mutex_lock(&student_mutex);
    if (waiting_students < NUM_OF_SEATS) { // seat is available
      waiting_students++; /** CS **/
      printf("student[%ld, %d]: takes a seat, waiting_students = %d\n", 
          s_id, get_help, waiting_students); 
      fflush(NULL);
      pthread_cond_signal(&student_cond); // notify TA
      //=>while(waiting_students==0){pthread_cond_wait(&student_cond)}
      pthread_mutex_unlock(&student_mutex);
      /** MUTEX_UNLOCK: pthread_cond_signal within mutex **/ 

      /** MUTEX_LOCK: pthread_cond_wait within mutex **/
      pthread_mutex_lock(&ta_mutex);
      while(avail_tas == 0) { // wait TA's help patiently
        pthread_cond_wait(&ta_cond, &ta_mutex); 
        //<=pthread_cond_signal(&ta_cond)
      }
      avail_tas--; /** CS **/
      pthread_mutex_unlock(&ta_mutex);
      /** MUTEX_LOCK: pthread_cond_wait within mutex **/
      printf("student[%ld, %d]: receives help\n", s_id, ++get_help); 
      fflush(NULL);
    } // EO-else
    else { // no seat available
      printf("student[%ld, %d]: will try later\n", s_id, get_help); 
      fflush(NULL);
      /** unlock mtex before programming **/
      pthread_mutex_unlock(&student_mutex);
    }
  } // EO-while
  pthread_exit(NULL);
}

/** 
 * If TA is not helping students, 
 * TA always (1) WAITS for the arrival of any student 
 * (If there is no student, TA is effectively taking a nap.) 
 *
 * After being notified by student's arrival, 
 * a napping TA wakes up and then 
 * (2) NOTIFIES the next student that TA becomes available 
 * and is ready to help. 
 *
 * After finishing (3) HELP with the current student,  
 * a TA keeps (1) WAITING; (2) NOTIFYING; and (3) HELPING
 */
void *tas_count(void *t)
{
  long t_id = (long) t;
  int offer_help = 0;
  int sec;

  /** a TA keeps (1) WAITING; (2) NOTIFYING; and (3) HELPING **/
  while (offer_help >= 0) { 

    /** MUTEX_LOCK: pthread_cond_wait within mutex **/
    pthread_mutex_lock(&student_mutex);
    while (waiting_students == 0) { // if no student, TA napping.
      pthread_cond_wait(&student_cond, &student_mutex);
      //<=pthread_cond_signal(&student_cond);
    }
    // TA wakes up by pthread_cond_signal(&student_cond)
    waiting_students--; /** CS **/
    pthread_mutex_unlock(&student_mutex);
    /** MUTEX_UNLOCK: pthread_cond_wait within mutex **/

    /** MUTEX_LOCK: pthread_cond_signal within mutex **/
    pthread_mutex_lock(&ta_mutex);                                                
    avail_tas++; /** CS **/
    // notify student ta is ready to help
    pthread_cond_signal(&ta_cond);
    //=>while(avail_tas==0){pthread_cond_wait(&ta_cond)}
    pthread_mutex_unlock(&ta_mutex);
    /** MUTEX_UNLOCK: pthread_cond_signal within mutex **/

    /* Heping outside of mutex */
    sec = sleeping_time();

    /*********************  CRUCIAL ****************************/
    /** MUTEX_LOCK: waiting_students (global shared variable) **/
    pthread_mutex_lock(&student_mutex);
    printf("TA[%ld, %d]: helps a student for %d seconds, waiting_students=%d\n", 
        t_id, ++offer_help, sec, waiting_students);  
    pthread_mutex_unlock(&student_mutex);
    /** MUTEX_UNLOCK: waiting_students (global shared variable) **/

    fflush(NULL);
    sleep(sec); // helping... 
  } // EO-while
  pthread_exit(NULL);
}

int main() 
{
  printf("CS149 NappingTA from Inhee Park (405)\n");
  fflush(NULL);

  /* 4 threads for students + 2 threads for TAs = 6 threads */
  int max_threads = NUM_OF_STUDENTS + NUM_OF_TAS;
  pthread_t threads[max_threads]; // thread ID
  long tIDs[] = {0, 1}; // TA thread ID and initialization
  long sIDs[] = {0, 1, 2, 3}; // Student thread ID and initialization

  /* thread attribute type and initialization */
  pthread_attr_t attr;
  pthread_attr_init(&attr);

  /* mutex initialization */
  pthread_mutex_init(&student_mutex, NULL);
  pthread_mutex_init(&ta_mutex, NULL);
  /* conditional variable initialization */
  pthread_cond_init(&student_cond, NULL);
  pthread_cond_init(&ta_cond, NULL);

  int t; // index for thread
  /* create threads */
  for (t = 0; t < max_threads; t++) {
    if (t < NUM_OF_TAS) // t = 0, 1 for TA threads
      pthread_create(&threads[t], &attr, tas_count, (void *) tIDs[t]);
    else // t = 2, 3, 4, 5 for Students threads
      pthread_create(&threads[t], &attr, students_count, (void *) sIDs[t-2]);
  }
  /* wait for termination of Students threads by join */
  for (t = 0; t < NUM_OF_STUDENTS; t++) 
    pthread_join(threads[t+2], NULL);
  /* once Students threads are all terminated, now cancel TA threads */
  for (t = 0; t < NUM_OF_TAS; t++) 
    pthread_cancel(threads[t]);
  printf("main(): done\n");
  /* destroy thread attribution, mutex and conditional variables */
  pthread_attr_destroy(&attr);
  pthread_mutex_destroy(&student_mutex);
  pthread_mutex_destroy(&ta_mutex);
  pthread_cond_destroy(&student_cond);
  pthread_cond_destroy(&ta_cond);
  return 0;
}


