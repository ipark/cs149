/*
 * hw0.3.c
 *
 *  Created on: Jan 28, 2019
 *      Author: ipark
 */

#include <stdio.h>

int main()
{
	// malloc the memory block
	int n = 100;
	int *int_a; // pointer to int
	int_a = (int *) malloc(sizeof(int) * n);

	// while-loop: initialization int block to 0-99
	int i = 0; // index
	while (i < n) {
		int_a[i] = i;
		i++;
	}

	// for-loop: print int
	for (i = 0; i < n; i++) {
		printf("%d\n", int_a[i]);
	}

	// free memory
	free(int_a);
	return 0;
}
