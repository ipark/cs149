/* CS149_HW2_Sudoku_Inhee Park_405 */
#include <stdio.h> // printf
#include <pthread.h> // pthread
#include <stdlib.h> // malloc(), free()
#include <stdint.h> // intptr_t

#define GRID_SIZE 9   // sudoku grid col/row dimension
#define SUB_GRID_SIZE 3 // sub-grid col/row dimension
#define NUM_THREADS 11  // max number of threads

int puzzle1[GRID_SIZE][GRID_SIZE] = { // given sudoku puzzle1
  {1,8,3,5,7,6,2,4,9}, 
  {4,6,7,1,2,9,3,5,8}, 
  {9,2,5,4,3,8,7,2,6}, 
  {8,7,2,3,5,1,6,9,4}, 
  {6,9,4,7,8,2,1,3,5}, 
  {3,5,1,6,9,4,8,7,2}, 
  {5,1,9,2,6,0,4,8,7}, 
  {7,4,6,8,1,5,9,2,3}, 
  {2,3,8,9,4,7,5,6,1}};
int puzzle2[GRID_SIZE][GRID_SIZE] = { // given sudoku puzzle2
  {5,9,6,7,1,4,3,2,8}, 
  {2,7,3,8,5,9,1,4,6}, 
  {8,1,4,2,3,6,7,5,9}, 
  {9,6,7,4,2,8,5,1,3}, 
  {1,2,5,3,9,7,6,8,4}, 
  {4,3,8,1,6,5,9,7,2}, 
  {3,5,1,6,8,2,4,9,7}, 
  {7,8,9,5,4,3,2,6,1}, 
  {6,4,2,9,7,1,8,3,5}};

typedef struct    // data structure for parameters set
{
  int *puzzle;  // pointer to the 9x9 multi-array puzzle
  int tid;    // thread id used for thread creation/join
  int row;    // starting row for sub-grid checker only
  int col;    // starting col for sub-grid checker only
} parameters;

/** to convenient malloc() and free() for multi-threads, 
 * used data structure as an array of the parameters type structure
 * so that can access by tid in the memory block
 */
parameters *data_array[NUM_THREADS]; 

/**
 * utility function to print out sudoku puzzle
 * @params: 
 *  p pointer to puzzle
 *  start_row starting index of row of the puzzle to print out
 *  start_col starting index of col of the puzzle to print out
 *  size width of the columns or rows to print out
 */
void print_puzzle(int *p, int start_row, int start_col, int size)
{ 
  int row, col;
  printf("\n GIVEN 9x9 PUZZLE\n -----------------\n");
  for (row = start_row; row < start_row + size; row++) {
    for (col = start_col; col < start_col + size; col++) {
      printf(" %d", *(p + row * GRID_SIZE + col));
    }
    printf("\n");
  }
   printf(" -----------------\n");
}


/**
 * redundancy check among digits 1-9 by sweeping the rows of a puzzle
 * @params
 *  targs thread argument as a parameters (structure) type data set
 */
void *row_checker(void *targs)
{
  parameters *data = (parameters *) targs;
  int tid = data->tid; // thread-id
  /** 
   * as a row validation code (0 is valid, -1 invalid)
   *  return this pointer when thread exits
   */
  void *validation_code = 0; 

  int row, col, val; // index for row/col of puzzle,  value of the element
  for (row = 0; row < GRID_SIZE; row++) {
    /** initialize the array for collection of digits 1-9 **/
    int uniq_chcker[GRID_SIZE] = {0,0,0,0,0,0,0,0,0};
    for (col = 0; col < GRID_SIZE; col++) { // sweeping columns of puzzle
      // get the value by accessing the element of the puzzle 
      val = *(data->puzzle + row * GRID_SIZE + col);
      /** if the retrived value from the puzzle is positive,
       *  that value is in 1-9 range, thus set 1 to the 
       *  corresponding element of the array
       */
      if (val > 0) uniq_chcker[val - 1] = 1;
    }
    for (col = 0; col < GRID_SIZE; col++) { // sweeping uniq_chcker array
      if (uniq_chcker[col] == 0) { // not found, i.e. missing
        printf(" ROW[%d], MISSING VALUE %d\n", row, col + 1);
        validation_code = (int *) -1; // invalid code -1
      }
    }
  }
  //printf("__exit%d", tid);
  pthread_exit((void *) validation_code); // thread exit with return pointer
}


/**
 * redundancy check among digits 1-9 by sweeping the columns of a puzzle
 * @params
 *  targs thread argument as a parameters (structure) type data set
 */
void *col_checker(void *targs)
{
  parameters *data = (parameters *) targs;
  int tid = data->tid; // thread-id
  /** 
   * as a col validation code (0 is valid, -1 invalid)
   *  return this pointer when thread exits
   */
  void *validation_code = 0;

  //printf("__start%d", tid);

  int row, col, val;
  for (col = 0; col < GRID_SIZE; col++) {
    /** initialize the array for collection of digits 1-9 **/
    int uniq_chcker[GRID_SIZE] = {0,0,0,0,0,0,0,0,0};
    for (row = 0; row < GRID_SIZE; row++) {
      // get the value by accessing the element of the puzzle 
      val = *(data->puzzle + row * GRID_SIZE + col);
      /** if the retrived value from the puzzle is positive,
       *  that value is in 1-9 range, thus set 1 to the 
       *  corresponding element of the array
       */
      if (val > 0) uniq_chcker[val - 1] = 1;
    }
    for (row = 0; row < GRID_SIZE; row++) {
      if (uniq_chcker[row] == 0) { // not found, i.e. missing
        printf(" COL[%d], MISSING VALUE %d\n", col, row + 1);
        validation_code = (int *) -1; // invalid code
      }
    }
  }
  //printf("__exit%d", tid);
  pthread_exit((void *) validation_code); // thread exit with return pointer
}

/**
 * redundancy check among digits 1-9 by sweeping a 3x3 subgrid
 * specified by the start_row and start_col within a 9x9 puzzle
 * @params
 *  targs thread argument as a parameters (structure) type data set
 */
void *subgrid_checker(void *targs)
{
  parameters *data = (parameters *) targs;
  int tid = data->tid; // thread id
  /** 
   * as a subgrid validation code (0 is valid, -1 invalid)
   *  return this pointer when thread exits
   */
  void *validation_code = 0;
  int start_row = data->row; // starting row of puzzle 
  int start_col = data->col; // starting col of puzzle 
  int row, col, val;
  //printf("__start%d", tid);
  /** initialize the array for collection of digits 1-9 **/
  int uniq_chcker[GRID_SIZE] = {0,0,0,0,0,0,0,0,0};
  // sweeping sub-row of puzzle for 3x3 sub-grid
  for (row = start_row; row < start_row + SUB_GRID_SIZE; row++) {
    // sweeping sub-columns of puzzle for 3x3 sub-grid
    for (col = start_col; col < start_col + SUB_GRID_SIZE; col++) {
      // get the value by accessing the element of the multi-array puzzle
      val = *(data->puzzle + row * GRID_SIZE + col);
      if (val < 1  || val > 9)  { // value is out of 1-9 range
        printf(" PUZZLE[%d][%d] = %d, NOT BETWEEN 1 AND 9\n", 
            col, row, val);
        validation_code = (int *) -1; // invalid code -1
      }
      else { // 1 <= val <= 9 (i.e. value is in 1-9 range, inclusive)
        if (uniq_chcker[val - 1] > 0) { // already found, i.e. duplicate
          printf(" PUZZLE[%d][%d] = %d, DUPLICAT VALUE\n", 
              col, row, val);
          validation_code = (int *) -1; // invalid code -1
        }
        else {
          uniq_chcker[val - 1] = 1; // if found, set it 1
        }
      }
    } // EO-for-col
  } // EO-for-row
  //printf("__exit%d", tid);
  pthread_exit((void *) validation_code); // thread exit with return pointer
}

/**
 * main job for sudoku validation
 * 1. display the given sudoku puzzle
 * 2. create 11 worker threads by for-loop to malloc(), pthread_create() per thread 
 * 3. join 11 worker threads by for-loop to retrive return validation_code from each thread
 *  followed by free()
 * 4. final decision to declare valid of invalid of the puzzle
 */
void sudoku(int *p) 
{
  // declare array of thread id's
  pthread_t threads[NUM_THREADS];

  // print the given puzzle
  print_puzzle((int *) p, 0, 0, GRID_SIZE);

  // creating 11 worker threads
  int t;
  for (t = 0; t < NUM_THREADS; t++) {
    //printf("__create_malloc%d", t); 
    //memory allocation per thread
	  data_array[t] = (parameters *) malloc(sizeof(parameters));
    if (data_array[t] != NULL) { // malloc() error handling
      data_array[t]->puzzle = (int *) p; // assign puzzle
      data_array[t]->tid = t; // assign thread id
    }
    // row-duplicate-checker    
    if (t == NUM_THREADS - 2) { // i.e. thread id 9
      pthread_create(&threads[t], NULL, row_checker, (void *)data_array[t]);
    }
    // col-duplicate-checker 
    else if (t == NUM_THREADS - 1) { // i.e. thread id 10
      pthread_create(&threads[t], NULL, col_checker, (void *)data_array[t]);
    }
    // 3x3-subgrid-chcker (thread id's 0 through 8)
    else {
      int start_row, start_col; //    t = 0, 1, 2, 3, 4, 5, 6, 7, 8
      start_row = (t / 3) * SUB_GRID_SIZE; // 0, 0, 0, 3, 3, 3, 6, 6, 6 
      start_col = (t % 3) * SUB_GRID_SIZE; // 0, 3, 6, 0, 3, 6, 0, 3, 6
      //printf("%d, %d\n", start_row, start_col);
      data_array[t]->row = start_row; // assign starting row of the given puzzle
      data_array[t]->col = start_col; // assign starting col of the given puzzle
      pthread_create(&threads[t], NULL, subgrid_checker, (void *)data_array[t]);
    } // EO-else (t = 0 thru 8)
  } // EO-for-loop
	
  // joining 11 threads
  long exit_status = 0; // for valid puzzle, sum of the validation_code should be 0
  void *validation_code; // return pointer from each thread
  for (t = 0; t < NUM_THREADS; t++) {
    pthread_join(threads[t], &validation_code);
    //printf("__join%d(code=%ld)", t, (intptr_t)validation_code);
    exit_status += (intptr_t)validation_code; // sum of the the integer pointer value
    //printf("__free%d", t);
    free(data_array[t]); // deallocate the memory per thread
  }
  if (exit_status == 0) 
    printf("==> PASS:::THIS PUZZLE IS VALID!\n");
  else // if exit_status is less than 0, there must be a problem in the puzzle
    printf("==> FAIL:::THIS PUZZLE IS NOT VALID!\n");

}

/**
 * simple main function to print out sudoku header
 * and testing two given sudoku puzzles by calling sudoku() function
 */
int main()
{
  printf(" CS149 Sudoku from Inhee Park (405)\n");

  sudoku((int *) puzzle1);
  sudoku((int *) puzzle2);
  return 0;
}
