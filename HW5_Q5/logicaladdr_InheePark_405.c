/**
* CS149-HW5-Q5. 
* Assume that a system has a 32-bit logical address 
* with N-KB page size (where N >= 1, and 1KB = 1024 bytes). 
* Write a C program that accepts two command line parameters, 
* the first one being the value of N (in decimal notation) 
* and the second one being a logical address in decimal 
* notation, and have it output the page size, the page 
* number and offset for the given logical address. For example, 
*   ./logicaladdr 4 19985 
* The program should output 
* Logical address translation by Inhee Park (405) 
* Page size = 4096, logical address 19985 => page number = 4, offset = 3601
* Test your program with the following four runs 
* ./logicaladdr 1 19981 
* ./logicaladdr 2 19982 
* ./logicaladdr 4 19984 
* ./logicaladdr 8 19988
**/
#include <stdio.h> // printf
#include <stdlib.h> // atoi, atol
#define KB_SIZE 1024 // Kilo-Byte
int main(int argc, char *argv[]) 
{
  /** 
   * ensure to check correct number of user input
   * otherwise exit(1)
   */
  if (argc != 3) {
    printf("\nUsage: ./logicaladdr 4       19985\n");
    printf("                    [page#] [logicalAddr]\n\n");
    printf("Make sure correct usage...now exiting\n\n");
    exit(1);
  }
  /**
   *    ./logicaladdr 4        19985 
   *    argv[0]      argv[1]  argv[2]
   */
  int N; /* first parameter of user input */
  /* page Number in decimal (to be determined)*/
  int pageNum; // QUOTIENT
  /* second parameter os user input */
  /* 32-bit logical addresss in decimal */
  int logicalAddr; // DIVIDEND 
  /* pageSize = N * KB_SIZE */
  int pageSize;  // DIVISOR
  /* logicalAddr = (pageNum)*(pageSize) + pageOffset */
  int pageOffset; // REMAINDER (to betermined)
  printf("Logical address translation by Inhee Park (405)\n");
  /* convert string data type to integer data type*/
  N = atoi(argv[1]); 
  logicalAddr = atoi(argv[2]);
  pageSize = N * KB_SIZE;
  /* logicalAddr = (pageNum)*(pageSize) + pageOffset */
  pageNum = logicalAddr / pageSize; // QUOTIENT = DIVIDEND / DIVISOR
  pageOffset = logicalAddr % pageSize; // REMAINDER = DIVIDEND % DIVISOR
  printf("Page size = %d, logical address %d => page number = %d, offset = %d\n\n",
      pageSize, logicalAddr, pageNum, pageOffset);
  return 0;
}

