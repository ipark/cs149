/* CS149 - HW1_Q1 - Inhee Park (405) */
#include <stdio.h>  // printf(3)
#include <unistd.h> // fork(2)

int main(void) 
{
    int i = 0; // initial value i
    if (fork() == 0) { /********** fork1 **********/
        ++i;
        if (fork() != 0) { /****** fork3 **********/
           i = 3;
        }
        else {         
           ++i; 
        }
        fork(); /***************** fork4 **********/
    }
    else {      
        i = 5;
        if (fork() == 0) { /****** fork2 **********/
            ++i;
        }
    }
    /* added a printf for process info retrieval  */
    printf("Bye pid=%d:i=%d\n", getpid(), i);
    return 0;
}
